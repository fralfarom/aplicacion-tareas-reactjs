# Front aplicación de tareas 

Aplicación de tareas construida con reactjs

## Instrucciones 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._



### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
1.Backend de la aplicación corriendo(urlBackend)
```

### Instalación 🔧

IMPORTANTE: para que funcione el proyecto primero debes correr el backend en el puerto 3000 y despues levantar esta aplicación en el puerto 3001 


###Tests
Para correr los tests integrados en la applicación debes ejecutar:
npm run cypress:open

```
1.git clone https://gitlab.com/fralfarom/aplicacion-tareas-reactjs.git
2.cd /aplicacion-tareas-reactjs
3.npm install
4.npm start
```
## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [React JS](https://es.reactjs.org/) - El framework web usado
* [MUI](https://mui.com/) - El framework de estilos utilizado
* [React-Redux](https://react-redux.js.org)

## Autor ✒️

* **Francisco Alfaro** - *Desarrollador* - [fralfarom](https://gitlab.com/fralfarom)

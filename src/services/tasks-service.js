import { apiDELETE, apiGET, apiPOST, apiPUT } from "./api"
const URL_API_TASK = process.env.REACT_APP_TASK_API

//Create a new task
export const newTaskService = async body => {
    return apiPOST(`${URL_API_TASK}tasks`, { "Content-Type": "application/json" }, body)
}

//Get tasks
export const getTasksService = async date => {
    return apiGET(`${URL_API_TASK}tasks${date ? '?endDate=' + date : ''}`)
}

//Get task by id
export const getTasksByIdService = async id => {
    return apiGET(`${URL_API_TASK}tasks/${id}`)
}

//Update a task by id
export const updateTaskService = async body => {
    return apiPUT(`${URL_API_TASK}tasks/${body.id}`, { "Content-Type": "application/json" }, body)
}

//Fiash a task by id
export const finishTaskService = async body => {
    return apiPUT(`${URL_API_TASK}tasks/${body.id}`, { "Content-Type": "application/json" }, body)
}

//Delete a task by id
export const deleteTaskService = async id => {
    return apiDELETE(`${URL_API_TASK}tasks/${id}`, null)
}

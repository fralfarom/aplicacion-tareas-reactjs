import axios from 'axios'

const responseBuilder = (data, status, message) => {
    return {
        status,
        message,
        data
    }
}

const headersBuilder = (customHeaders) => {
    return {
        ...customHeaders
    }
}

const requestOptionsBuilder = (method, customHeaders, body) => {
    const requestOptions = {
        method
    }

    if(customHeaders) requestOptions.headers = headersBuilder(customHeaders)

    return requestOptions
}

export const apiGET = async (url, customHeaders) => {
    let status, data, message

    try {
        const requestOptions = requestOptionsBuilder("GET")

        const fetchResponse = await axios.get(url, requestOptions)
        status = fetchResponse.status

        data = await fetchResponse.data
        message = "OK"
    } catch (error) {
        status = -1
        data = error
        message = "NOK"
    }
    return responseBuilder(data, status, message)
}

export const apiPOST = async (url, customHeaders = {}, body) => {
    let status, data, message

    try {
        customHeaders = { ...customHeaders }

        const requestOptions = requestOptionsBuilder("POST", customHeaders, body)
        const fetchResponse = await axios.post(url, body, requestOptions)
        status = fetchResponse.status

        data = await fetchResponse.data
        message = "OK"
    } catch (error) {
        status = -1
        data = error
        message = "NOK"
    }

    return responseBuilder(data, status, message)
}

export const apiPUT = async (url, customHeaders = {}, body) => {
    let status, data, message

    try {
        customHeaders = { ...customHeaders }

        const requestOptions = requestOptionsBuilder("PUT", customHeaders, body)
        const fetchResponse = await axios.put(url, body, requestOptions)
        status = fetchResponse.status

        data = await fetchResponse.data
        message = "OK"
    } catch (error) {
        status = -1
        data = error
        message = "NOK"
    }

    return responseBuilder(data, status, message)
}

export const apiDELETE = async (url, customHeaders = {}, body) => {
    let status, data, message

    try {
        customHeaders = { ...customHeaders }

        const requestOptions = requestOptionsBuilder("DELETE", customHeaders, body)
        const fetchResponse = await axios.delete(url, requestOptions)
        status = fetchResponse.status

        data = await fetchResponse.data
        message = "OK"
    } catch (error) {
        status = -1
        data = error
        message = "NOK"
    }

    return responseBuilder(data, status, message)
}
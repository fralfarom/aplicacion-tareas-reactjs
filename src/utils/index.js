//Filter tasks by date or description
export const filterTasksUtil = (tasks, date, desc) => {
    return tasks.map(val => {
        const endDate = val.endDate

        if (date && desc) {
            if (endDate === date && val.description.includes(desc)) return val
        } else if (!date) {
            if (val.description.includes(desc)) return val
        } else if (!desc) {
            if (endDate === date) return val
        }

        return undefined
    }).filter(val => val !== undefined)
}

//Check if a task going to expire
export const checkExpire = tasks => {
    return tasks.map(val => {
        const endDate = new Date(val.endDate)
        const currentDate = new Date()
        if (calculateDaysBetween(endDate, currentDate) <= 1 && calculateDaysBetween(endDate, currentDate) > -1) {
            val.status = 'expiring'
            return val
        }
        if (endDate < currentDate && val.status === 'active') {
            val.status = 'expired'
            return val
        }

        return val
    })
}

//Sort task by createDate, endDate, status
export const sortBy = (tasks, sort) => {
    switch (sort) {
        case 'endDate':
            return tasks.sort((a, b) => Date.parse(a.endDate) - Date.parse(b.endDate))
        case 'status':
            return tasks.sort((a, b) => statusImportance[a.status] - statusImportance[b.status])
        default:
            return tasks.sort((a, b) => Date.parse(b.createDate) - Date.parse(a.createDate))
    }
}

export const statusColorCard = {
    completed: '#31ce08',
    new: '#2a2b4a',
    active: '#2a2b4a',
    expired: '#ae0101'
}

export const errorMessageBuilder = errors => {
    console.log('errors: ', errors);
    const baseMessage = 'Favor completar los siguientes campos:'
    const errMessages = Object.keys(errors).map(err => {
        if (errors[err]) return err
        return undefined
    }).filter(val => val !== undefined)

    return `${baseMessage} ${errMessages.join(', ')}`
}

const calculateDaysBetween = (day1, day2) => {
    const timeDate = day1.getTime() - day2.getTime()
    return Number.parseInt(timeDate / (1000 * 3600 * 24))
}

const statusImportance = {
    expiring: 1,
    active: 2,
    completed: 3,
    expired: 4
}
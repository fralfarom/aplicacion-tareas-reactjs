import React, { useState, useEffect } from 'react'
import Stack from '@mui/material/Stack'
import { CardComponent } from '../card'
import { ModalComponent } from '../modal'
import Box from '@mui/material/Box'
import { deleteTaskService, finishTaskService, getTasksService } from '../../services/tasks-service'
import CleaningServicesIcon from '@mui/icons-material/CleaningServices'
import FilterAltIcon from '@mui/icons-material/FilterAlt'
import { ClickableIcon } from '../clickableIcon'
import { CreateTaskComponent } from '../createTask'
import { PoperComponent } from '../poper'
import { FilterTasksComponent } from '../filter'
import { TemplateComponent } from '../modal/template'
import { checkExpire, sortBy, statusColorCard } from '../../utils'
import { styled } from '@mui/material/styles'

const MainContainer = styled(Box)(({ theme }) => ({
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'column',
  minHeight: '100vh',
  background: '#1b203d'
}))

const HeaderContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  justifyContent: 'space-between',
  width: '60%',
  marginTop: '20px'
}))

const CardsContainer = styled(Stack)(({ theme }) => ({
  width: '60%',
  marginBottom: '20px'
}))

const DateFilterContainer = styled(Box)(({ theme }) => ({
  lineHeight: 0
}))

export const MainComponent = () => {
  const [open, setOpen] = useState(false)
  const [selectedValue, setSelectedValue] = useState()
  const [date, setDate] = useState('')
  const [modalName, setModalName] = useState()
  const [tasks, setTasks] = useState([])
  const [currentTasks, setCurrentTasks] = useState([])
  const [checkeds, setCheckeds] = useState([])
  const [modalData, setModalData] = useState()
  const [actionName, setActionName] = useState()

  const getTasks = async (date, updateStatus) => {
    console.log(date);
    setModalName('loader')
    setOpen(true)
    setTasks([])
    setCurrentTasks([])

    const serviceResponse = await getTasksService(date)

    if (serviceResponse.status !== 200 && serviceResponse.status !== 201) return setModalName('error')

    //check if a task is expire
    const mapedTasks = checkExpire(serviceResponse.data)

    //sort task by default
    const sortedTasks = sortBy(mapedTasks)

    setTasks([...sortedTasks])
    setCurrentTasks([...sortedTasks])

    //Show success from update a task by its status
    if (updateStatus) return setModalName('success')


    setOpen(false)
  }

  const deleteTask = async () => {
    const serviceResponse = await deleteTaskService(modalData.id)

    if (serviceResponse.status !== 200 && serviceResponse !== 201) return setModalName('error')

    return getTasks(null, true)
  }

  const handleClickOpen = (newModalName, data) => {
    setOpen(true)
    setModalName(newModalName)
    setModalData(data)
    setActionName(data?.actionName)
  }

  const finishTask = async () => {
    const body = {
      title: modalData.title,
      description: modalData.description,
      endDate: modalData.endDate,
      createDate: modalData.createDate,
      status: "completed",
      id: modalData.id
    }

    const serviceResponse = await finishTaskService(body)
    if (serviceResponse.status !== 200 && serviceResponse !== 201) return setModalName('error')

    return getTasks(null, true)
  }

  const sortTasks = type => {
    const sortedTasks = sortBy(currentTasks, type)
    setCurrentTasks([...sortedTasks])
  }

  const handleClose = (value) => {
    setOpen(false)
    setSelectedValue(value)
  }

  const onDateChange = e => {
    setDate(e.target.value)
  }

  useEffect(() => {
    //Get default tasks
    getTasks()
  }, [])

  useEffect(() => {
    console.log(date);
    if (date.length > 0) getTasks(date)
  }, [date])

  return (
    <MainContainer>
      <HeaderContainer id='header-container'>
        < ClickableIcon id='clear-btn' icon={CleaningServicesIcon} onClick={e => getTasks()} color='rgb(215, 215, 215)' size='30px' />
        <h1 style={{
          color: '#d7d7d7',
          margin: '0 20px 20px 60px'
        }}>Lista de Tareas</h1>
        <DateFilterContainer>
          <p style={{ fontSize: '12px', color: 'rgb(215, 215, 215)' }}>Filtrar Fecha:</p>
          <input type="date"
            onChange={onDateChange}
            name="date"
            className="Input-Date" />
        </DateFilterContainer>
        <Box>
          <ClickableIcon id='filter-btn' icon={FilterAltIcon} color='rgb(215, 215, 215)' onClick={() => handleClickOpen('filter')} size='30px' />
          <PoperComponent sortTasks={sortTasks} />
        </Box>
      </HeaderContainer>
      <CardsContainer spacing={2}>
        {currentTasks.map((val, idx) => (
          <CardComponent key={idx}
            data={val}
            checkeds={checkeds}
            openModal={handleClickOpen}
            setCheckeds={setCheckeds}
            border={`1.5px solid ${statusColorCard[val.status]}`} />
        ))}
        <CardComponent name={'new'} openModal={handleClickOpen} />
      </CardsContainer>
      {/* Reset Modal */}
      {open ?
        <ModalComponent selectedValue={selectedValue} open={open} onClose={handleClose}>
          {modalName === 'create' ?
            <CreateTaskComponent setName={setModalName} getTasks={getTasks} updateData={modalData} name='create' /> :
            modalName === 'filter' ?
              <FilterTasksComponent tasks={tasks} setTasks={setCurrentTasks} /> :
              modalName === 'update' ?
                <CreateTaskComponent name='update' setName={setModalName} getTasks={getTasks} updateData={modalData} /> :
                <TemplateComponent name={modalName} actionName={actionName} onConfirm={{
                  delete: deleteTask,
                  finish: finishTask
                }} closeModal={handleClose} />
          }
        </ModalComponent>
        : null}
    </MainContainer>
  )
}

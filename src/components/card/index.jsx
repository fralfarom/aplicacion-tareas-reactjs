import React from 'react'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import { styled } from '@mui/material/styles'
import { TemplateCardCompontent } from './template'

const StyledCard = styled(Card)(({ theme }) => ({
    padding: theme.spacing(1),
    minWidth: '100%',
    boxShadow: '2px 2px 23px #020d22',
    background: '#2a2b4a'
}))

const StyledCardContent = styled(CardContent)(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    minWidth: '99%',    
}))

export const CardComponent = ({ name, openModal, data, checkeds, setCheckeds, border }) => {
    return (
        <StyledCard sx={{border: border}} id={name}>
            <StyledCardContent>
                <TemplateCardCompontent name={name} openModal={openModal} data={data} checkeds={checkeds} setCheckeds={setCheckeds} />
            </StyledCardContent>
        </StyledCard>
    )
}

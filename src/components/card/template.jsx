import React from 'react'
import Checkbox from '@mui/material/Checkbox'
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'
import AddIcon from '@mui/icons-material/Add'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import UpdateIcon from '@mui/icons-material/Update'
import { InputCompontent } from '../input'
import { ClickableIcon } from '../clickableIcon'
import Box from '@mui/material/Box'

export const TemplateCardCompontent = ({ name, openModal, data, setCheckeds, checkeds }) => {

    const onValueChange = e => {
        if (e.target.checked) {
            setCheckeds(checkeds.concat(e.target.value))
        } else {
            setCheckeds(checkeds.filter(val => val !== e.target.value))
        }
    }

    switch (name) {
        case 'new':
            return (
                <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }} onClick={() => openModal('create')}>
                    <ClickableIcon icon={AddIcon} color={'white'} />
                </Box>
            )
        default:
            return (
                <>
                    <Box sx={{ display: 'flex', width: '60%' }}>
                        <Checkbox size='medium' sx={{ color: '#999', height: 'auto' }} onChange={onValueChange} value={data.id} />
                        <Box>
                            <h2 style={{ marginBottom: '2px', color: 'rgba(234, 234, 234, 0.87)' }} onClick={e => console.log(checkeds)}>{data.title}</h2>
                            <p style={{ color: '#999' }}>{data.description}</p>
                        </Box>
                    </Box>
                    <Box sx={{ display: 'flex', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around', width: '40%' }}>
                        <InputCompontent placeholder={'Fecha termino'} type="date" disabled={true} value={data.endDate} color='rgb(153, 153, 153)' />
                        <Box sx={{ display: 'flex', flexDirection: 'column', paddingRight: '20px' }}>
                            <ClickableIcon icon={UpdateIcon} color={'#7171db'} onClick={() => openModal('update', data)} />
                            <ClickableIcon icon={CheckCircleOutlineIcon} color={'#70b570'} onClick={() => openModal('confirm', { ...data, actionName: 'finish' })} />
                            <ClickableIcon icon={HighlightOffIcon} color={'#ec7373'} onClick={() => openModal('confirm', { ...data, actionName: 'delete' })} />
                        </Box>
                    </Box>
                </>
            )
    }
}

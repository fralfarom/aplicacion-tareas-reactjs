import React from 'react'
import { Box, Typography } from '@mui/material'
import { styled } from '@mui/material/styles'

const ItemList = styled(Typography)(({ theme }) => ({
  borderBottom: '1px solid #e3e3e3',
  fontSize: '13px',
  padding: '5px',
  '&:hover': {
    cursor: 'pointer'
  }
}))

export const SortComponent = ({ sortTasks }) => {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', fontSize: '12px', background: 'white' }}>
      <ItemList variant='p' onClick={e => sortTasks('createDate')}>Ordenar por Fecha Creación</ItemList>
      <ItemList variant='p' onClick={e => sortTasks('endDate')}>Ordenar por Fecha Expiración</ItemList>
      <ItemList variant='p' onClick={e => sortTasks('status')}>Ordenar por Estado</ItemList>
    </Box>
  )
}

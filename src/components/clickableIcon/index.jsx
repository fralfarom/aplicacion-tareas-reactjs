import React from 'react'
import { styled } from '@mui/material/styles'

export const ClickableIcon = ({ icon, color, onClick, size, id }) => {
    const Clickable = styled(icon)({
        '&:hover': {
            background: '#b9b9b91c',
            border: 0,
            borderRadius: '40px',
            cursor: 'pointer',

        }
    })
    return (
        <div id={id}>
            <Clickable sx={{ fontSize: size || '50px', color }} onClick={onClick} />
        </div>
    )
}

import React from 'react'
import Dialog from '@mui/material/Dialog'

export const ModalComponent = ({children, onClose, selectedValue, open }) => {

    const handleClose = () => {
        onClose(selectedValue)
    }

    return (
        <Dialog onClose={handleClose} open={open} fullWidth>
            {children}            
        </Dialog>
    )
}
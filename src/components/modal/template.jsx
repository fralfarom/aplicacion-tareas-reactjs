import React from 'react'
import CircularProgress from '@mui/material/CircularProgress'
import Box from '@mui/material/Box'
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import Button from '@mui/material/Button'
import HelpOutlineIcon from '@mui/icons-material/HelpOutline'
import { styled } from '@mui/material/styles'

const ConfirmActions = styled(Box)(({ theme }) => ({
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    gap: '25px'
}))


export const TemplateComponent = ({ name, onConfirm, closeModal, actionName }) => {
    return (
        <Box sx={{
            display: 'flex',
            minHeight: '400px',
            flexDirection: 'column',
            alignItems: 'center',
            paddingTop: '90px'
        }}>
            {name === 'loader' ?
                <>
                    <CircularProgress />
                    <h3>Procesando...</h3>
                </>
                : name === 'error' ?
                    <>
                        <HighlightOffIcon sx={{ fontSize: '100px', color: '#ec7373' }} />
                        <h3>Há ocurrido un error</h3>
                    </>
                    : name === 'confirm' ?
                        <>
                            <HelpOutlineIcon sx={{ fontSize: '100px', color: '#ecc273' }} />
                            <h3>¿Estas seguro?</h3>
                            <ConfirmActions>
                                <Button variant="outlined" size="medium" onClick={closeModal}>Cancelar</Button>
                                <Button variant="contained" size="medium" onClick={onConfirm[actionName]}>Confirmar</Button>
                            </ConfirmActions>
                        </> :
                        <>
                            <CheckCircleOutlineIcon sx={{ fontSize: '100px', color: '#70b570' }} />
                            <h3>Operación realizada con exito!!</h3>
                        </>
            }
        </Box>
    )
}

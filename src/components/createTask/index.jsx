import React, { useEffect, useState } from 'react'
import { InputCompontent } from '../input'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import DialogTitle from '@mui/material/DialogTitle'
import { getTasksByIdService, newTaskService, updateTaskService } from '../../services/tasks-service'
import { errorMessageBuilder } from '../../utils'
import { styled } from '@mui/material/styles'

const CreateTaskContainer = styled(Box)(({ theme }) => ({
    padding: '0 0 20px 20px',
    lineHeight: '50px'
}))

const SubmitContainer = styled(Box)(({ theme }) => ({
    width: '95%',
    display: 'flex',
    justifyContent: 'end',
    padding: '1%',
    marginTop: 2
}))



export const CreateTaskComponent = ({ setName, getTasks, name, updateData }) => {
    const [form, setForm] = useState({
        title: '',
        description: '',
        endDate: ''
    })
    const [errors, setErrors] = useState({
        title: false,
        description: false,
        endDate: false
    })

    const onFormChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const sendTask = async () => {
        const currentDate = new Date()
        const body = {
            ...form,
            "createDate": currentDate.toLocaleString(),
            status: 'active'
        }

        // If updating add ID to body
        if (name === 'update') body.id = updateData.id

        setName('loader')


        const apiResponse = name === 'update' ? await updateTaskService(body) : await newTaskService(body)

        if (apiResponse.status === 201 || apiResponse.status === 200) setName('success')
        if (apiResponse.status !== 201 && apiResponse.status !== 200) return setName('error')

        getTasks(null, name === 'update' ? true : false)
    }

    const formValidation = () => {
        let valid = true
        const errorCopy = errors

        for (const val of Object.keys(form)) {
            if (!form[val]) {
                errorCopy[val] = true
                valid = false
            } else {
                errorCopy[val] = false
            }
        }

        setErrors({ ...errorCopy })
        return valid
    }

    const submit = () => {
        if (!formValidation()) return alert(errorMessageBuilder(errors))

        sendTask()
    }

    useEffect(() => {
        const searchTaskData = async taskId => {
            const serviceResponse = await getTasksByIdService(taskId)
            const task = serviceResponse.data
            setForm({
                title: task.title,
                description: task.description,
                endDate: task.endDate
            })
        }

        if (updateData) searchTaskData(updateData.id)
    }, [updateData])

    return (
        <>
            <DialogTitle>{name === 'update' ? 'Actualizar' : 'Crear'} una tarea</DialogTitle>
            <CreateTaskContainer>
                <Box sx={{ display: 'flex' }}>
                    <InputCompontent color='rgba(23, 23, 23, 0.6)' name="title" value={form.title} onChange={onFormChange} placeholder={'Nombre de la tarea'} />
                    <InputCompontent color='rgba(23, 23, 23, 0.6)' name="endDate" value={form.endDate} onChange={onFormChange} type="date" placeholder={'Fecha de la tarea'} />
                </Box>
                <InputCompontent color='rgba(23, 23, 23, 0.6)' name="description" value={form.description} onChange={onFormChange} placeholder={'Descripción de la tarea'} inputWidth='270%' />
                <SubmitContainer>
                    <Button variant="outlined" size="medium" onClick={submit}>{name === 'update' ? 'ACTUALIZAR' : 'CREAR'}</Button>
                </SubmitContainer>
            </CreateTaskContainer>
        </>
    )
}

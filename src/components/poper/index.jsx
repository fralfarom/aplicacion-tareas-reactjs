import React from 'react'
import Popover from '@mui/material/Popover'
import { SortComponent } from '../sort'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'

export const PoperComponent = ({ sortTasks }) => {

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <>
            <MoreHorizIcon onClick={handleClick} sx={{ color: 'white' }} />
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <SortComponent sortTasks={sortTasks} />
            </Popover>
        </>
    )
}

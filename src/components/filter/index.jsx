import React, { useState } from 'react'
import { InputCompontent } from '../input'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import DialogTitle from '@mui/material/DialogTitle'
import { filterTasksUtil } from '../../utils'
import { styled } from '@mui/material/styles'

const FilterTaskContainer = styled(Box)(({ theme }) => ({
    padding: '0 0 20px 20px',
    display: 'flex'
}))

const SubmitContainer = styled(Box)(({ theme }) => ({
    width: '95%',
    display: 'flex',
    justifyContent: 'end',
    marginTop: 2,
    marginBottom: 2
}))

export const FilterTasksComponent = ({ tasks, setTasks }) => {

    const [form, setForm] = useState({
        date: '',
        description: ''
    })

    const onFormChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const filterTasks = async () => {
        const filteredTasks = filterTasksUtil(tasks, form.date, form.description)
        setTasks([...filteredTasks])
    }


    const formValidation = () => {
        if (!form.date && !form.description) {
            return false
        }
    }

    const submit = () => {
        if (!formValidation()) return alert('Ingrese por lo menos 1 filtro')

        filterTasks()
    }

    return (
        <>
            <DialogTitle>Filtrar Tareas</DialogTitle>
            <FilterTaskContainer>
                <Box sx={{ width: '50%', mt: 2 }}>
                    <InputCompontent color='rgba(23, 23, 23, 0.6)' name="date" value={form.date} onChange={onFormChange} type="date" placeholder={'Fecha de la tarea'} />
                </Box>
                <Box sx={{ width: '50%' }}>
                    <InputCompontent color='rgba(23, 23, 23, 0.6)' name="description" value={form.description} onChange={onFormChange} placeholder={'Descripción de la tarea'} />
                </Box>

            </FilterTaskContainer>
            <SubmitContainer>
                <Button variant="outlined" size="medium" onClick={submit}>Filtrar</Button>
            </SubmitContainer>
        </>
    )
}

import React from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import CalendarTodayIcon from '@mui/icons-material/CalendarToday'

export const InputCompontent = ({ type = "text", placeholder, inputWidth = 'auto', name, onChange, value, color = 'rgba(234, 234, 234, 0.87)', disabled }) => {
    return (
        <Box sx={{ width: '100%' }}>
            {type === "date" ? <CalendarTodayIcon sx={{ mr: 1, my: 1, color }} /> : null}
            <TextField id={name}
                type={type}
                variant="standard"
                name={name}
                value={value}
                onChange={onChange}
                disabled={disabled}
                label={type === 'date' ? null : placeholder}
                InputProps={{
                    sx: { color, width: inputWidth, WebkitTextFillColor: disabled ? 'white' : '' }
                }} />
        </Box>
    )

}

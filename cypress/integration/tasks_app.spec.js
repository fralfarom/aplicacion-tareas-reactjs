describe('Note App', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3001/')
    })

    it('Health check', () => {
        cy.contains('Lista de Tareas')
    })

    it('Filter modal can be opened', () => {
        cy.get('[id="filter-btn"]').click()
        cy.contains('Filtrar Tareas')
    })

    it('Create task modal can be opened', () => {
        cy.get('[id="new"]').click()
        cy.contains('Crear una tarea')
    })

    it('A task can be created', () => {
        //Open modal
        cy.get('[id="new"]').click()
        cy.contains('Crear una tarea')

        //Create task
        cy.get('[id="title"]').type('Tarea desde Cypress')
        cy.get('[id="description"]').type('Esta tarea es una tarea de prueba creada desde cypress')
        cy.get('[id="endDate"]').click().type('2022-02-28')
        cy.contains('CREAR').click()

        //Check if task was created
        cy.contains('Tarea desde Cypress')
    })

    it('Update task modal can be opened', () => {
        cy.get('[data-testid="UpdateIcon"]').first().click()
        cy.contains('Actualizar una tarea')
    })

    it('A task can be updated', () => {
        //Open modal
        cy.get('[data-testid="UpdateIcon"]').first().click()
        cy.contains('Actualizar una tarea')

        //Update task
        cy.get('[id="title"]').clear().type('Tarea actualizada desde Cypress')
        cy.get('[id="description"]').clear().type('Esta tarea es una tarea actualizada desde cypher')
        cy.get('[id="endDate"]').clear().click().type('2022-02-28')
        cy.contains('ACTUALIZAR').click()

        //Check if task was updated
        cy.contains('Tarea actualizada desde Cypress')
    })
})